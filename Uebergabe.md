Einige Dokumente sollen für die Übergabe vorbereitet werden. Deren Übergabe
und Integration in den Mietvertrag soll geklärt werden.

Weiter geht es hier um den Zustand der Wohnung zur Übergabe.

Inventarliste
===

Die Inventarliste soll dem Mieter mit der Wohnung übergeben werden.

  * Bilder. Etwa können wir damit die Ausstattung des Bades zeigen.
  * Mindestens eine Zeichnung der Küche.
  * Eine Liste die zumindest die losen Gegenstände in der Wohnung enthält.

**TODO** übereinkommen, wie das mit den Bildern funktionieren soll.
Werden wir einseitig eine Menge Bilder reichen und uns vom Mieter
quittieren lassen, dass die Bilder den Zustand der Wohnung bei
der Übergabe zeigen. Ich finde das Verfahren unkomfortabel.
Besser wäre es, die Wohnung mit ihm zusammen zu begehen und dabei
in den Bildern zu blättern. Damit wäre das Übergabe-Medium die
Menge der ausgedruckten Bilder. (Deren Anzahl würden wir im
Protokoll festhalten.)

Bilder dienen entweder unserer eigenen Dokumentation
oder sie sind Teil der Übergabe-Prozedur. Bilder als Teil unserer
Dokumentation werden wir nicht weitergeben. Wie wir die für uns
archivieren, können wir noch überlegen.

Die Inventarliste beinhält

  * die Ausstattung der Naß-Räume (in Bildern),
  * die Ausstattung der Küche (in einer Zeichnung),
  * die Kommode vor der Wohnung,
  * die Bretter auf den Heizungen,
  * das Regal im Keller.

**TODO** Was noch?

**TODO RA:** Das Übergabe-Protokoll kann ein (vom Mietvertrag) separates
Dokument sein. Es wird von beiden Parteien unterschrieben. Ist das so?

Pflege-Hinweise
===

Das betrifft

  * die Küche und
  * das Schlafzimmerfenster.

Zur Küche wird es Aufkleber geben, eventuell unsere Dokumentation und
auf jeden Fall die
Dokumentation, die wir bei der Übergabe der Küche bekommen haben.

Mehr Pflege-Info zur Küche findet sich in der Dokumentation zu
den Geräten, die wir dem Mieter geben wollen.

Zum Schlafzimmerfenster: Die Hersteller-Dokumentation ist in
Mamis Haus. Ich werde sagen, wo Du sie findest. Die verweist auf
http://www.roto-extra.com/ --. Von dort können ausführlichere
Informationen heruntergeladen werden. Der Beipackzettel ist nicht
allzu ausführlich.

**TODO** Dokumentation finden. Dokumentation herunterladen und
eventuell ausdrucken.

Dokumentation zu den Küchengeräten
===

Ich schlage vor, dass wir die Originale mitgeben und vorher
sicherstellen, dass wir entweder Kopien haben oder welche von den
Herstellern heruntergeladen haben.

Die Dokumente wollen wir in die Inventar-Liste aufnehmen.

**TODO** Volker: Digitale Versionen der Handbücher finden. Ulrike:
Falls etwas bleibt, das man nicht herunterladen kann: Einscannen
der Dokumente, für unser Archiv. Sehr wahrscheinlich bleibt da
nichts.

Die Dokumente sind wahrscheinlich in einem Fach in der Küche.
Möglicherweise sind die Dokumente zum Kühlschrank im Kühlschrank.

**DONE** Dokumente heruntergeladen. Die vom Kühlschrank fehlen noch.

Zustand der Wohnung und der Ausstattung
===

Zum Zustand der Geräte in der Küche: Der Ofen ist eingebrannt.
Die Spülmaschine hat ihren initialen Spülgang erlebt. Sie ist auf
die Wasserhärte eingestellt. Die Wasserhärte ist 3. Das wird der
Mieter wissen wollen, wenn er auf ein enthärtendes
Reinigungsmittel umstellen möchte.

Das Waschbecken im Bad hat eine Macke.

Die Eingangstür erwartet einen Einsatz vom Schreiner.

Der kann auch die Falle gängig machen. Jetzt ist es immer etwas
mühsam und laut, die Tür ohne Schlüssel und ohne Klinke zu schließen.

**TODO** Ist der Keller ausgefegt?

Das Holz an den Wänden hat Spuren von falscher Behandlung.

Die Fugen im Boden im Eingangsbereich sind notdürftig repariert.
Sie werden sich wieder öffnen, besonders so weit sie nicht mit
Silikon gedichtet sind.

Das Rollo am Fenster zur Kirche hin hat einen kleinen Defekt. Man
könnte das nähen.

Das Holz der Pergola ist effektiv unbehandelt.

Warn-Hinweise
===

Die Aufkleber am Dachboden.

Es geht um die Belastbarkeit des Dachbodens und darum, dass diese
Tür nicht frei heruntergeklappt werden kann.

Hausordnung, Pflichten des Mieters
===

Die Heizung muss regelmäßig entlüftet werden. Ulle, Du fragst
die Frau Stahl, wie und wann der Mieter das machen soll.
Besonders ist hier wahrscheinlich, dass ein Mieter die Entlüftung
für das ganze Haus vornimmt.

In der Wohnung darf nicht geraucht werden.

**TODO RA:** Wie bekommen wir die Hausordnung, die Pflichten und unser
Rauchverbot in den Mietvertrag?
